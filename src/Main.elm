module Main exposing (Model, Msg(..), init, main, update, view)

import Browser exposing (Document)
import Browser.Navigation as Nav
import Data.Artist as Artist exposing (Artist)
import Data.Id as Id
import Data.Song as Song exposing (Song)
import Data.Status as Status exposing (Status(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Page.Artist as Artist
import Page.Home as Home exposing (Msg(..))
import Page.NotFound as NotFound
import Route exposing (Route(..))
import Url exposing (Url)
import Util.List as List



-- MAIN


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = ChangedUrl
        , onUrlRequest = OnUrlRequest
        , subscriptions = subscriptions
        , update = update
        , view = view
        }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- MODEL


type alias Model =
    { page : Page
    , key : Nav.Key
    }


{-| TODO: Add Redirect to handle init state
-}
type Page
    = Artist Artist.Model
    | Home Home.Model
    | NotFound


init : () -> Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    -- FIXME: update init state from NotFound to something more sensible
    changeRouteTo (Route.fromUrl url)
        { page = NotFound
        , key = key
        }



-- UPDATE


type Msg
    = ChangedUrl Url
    | OnUrlRequest Browser.UrlRequest
    | GotPageMsg PageMsg


type PageMsg
    = GotArtistMsg Artist.Msg
    | GotHomeMsg Home.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangedUrl url ->
            changeRouteTo (Route.fromUrl url) model

        OnUrlRequest (Browser.Internal url) ->
            ( model
            , Nav.pushUrl model.key (Url.toString url)
            )

        OnUrlRequest (Browser.External href) ->
            ( model, Nav.load href )

        GotPageMsg pageMsg ->
            case ( pageMsg, model.page ) of
                ( GotHomeMsg subMsg, Home home ) ->
                    Home.update subMsg home
                        |> updateWith Home GotHomeMsg model

                ( GotArtistMsg subMsg, Artist artist ) ->
                    Artist.update subMsg artist
                        |> updateWith Artist GotArtistMsg model

                ( _, _ ) ->
                    -- Disregard messages that arrived for the wrong page.
                    ( model, Cmd.none )


updateWith :
    (subModel -> Page)
    -> (subMsg -> PageMsg)
    -> Model
    -> ( subModel, Cmd subMsg )
    -> ( Model, Cmd Msg )
updateWith toModel toMsg model ( subModel, subCmd ) =
    ( { model | page = toModel subModel }
    , Cmd.map (GotPageMsg << toMsg) subCmd
    )


changeRouteTo : Route -> Model -> ( Model, Cmd Msg )
changeRouteTo route model =
    case route of
        Route.Home ->
            Home.init
                |> updateWith Home GotHomeMsg model

        Route.Artist artistId ->
            Artist.init artistId
                |> updateWith Artist GotArtistMsg model

        Route.NotFound ->
            -- FIXME: Implement not found page
            ( { model | page = NotFound }, Cmd.none )



-- VIEW


view : Model -> Document Msg
view model =
    let
        viewPage toMsg config =
            let
                { title, body } =
                    config
            in
            { title = title
            , body = List.map (Html.map toMsg) body
            }
    in
    case model.page of
        Artist artist ->
            viewPage (GotPageMsg << GotArtistMsg) (Artist.view artist)

        Home home ->
            viewPage (GotPageMsg << GotHomeMsg) (Home.view home)

        NotFound ->
            -- FIXME: Create NotFound page
            { title = "Not found", body = [ text "Not found" ] }
