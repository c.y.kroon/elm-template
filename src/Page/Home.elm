module Page.Home exposing (Model, Msg(..), init, update, view)

import Data.Artist as Artist exposing (Artist)
import Data.Song as Song exposing (Song)
import Data.Status as Status exposing (Status(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Route
import Util.List as List



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- MODEL


type alias Model =
    { artists : Status Http.Error (List Artist)
    , searchTerm : String
    , songs : Status Http.Error (List Song)
    }


init : ( Model, Cmd Msg )
init =
    ( { artists = Loading
      , searchTerm = ""
      , songs = Loading
      }
    , Cmd.batch
        [ Artist.getCollection GotArtists
        ]
    )



-- UPDATE


type Msg
    = NoOp
    | GotArtists (Result Http.Error (List Artist))
    | GotSongs (Result Http.Error (List Song))
    | OnSearchTermInput String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        GotArtists result ->
            ( { model | artists = Status.fromResult result }
            , Cmd.none
            )

        GotSongs result ->
            ( { model | songs = Status.fromResult result }
            , Cmd.none
            )

        OnSearchTermInput searchTerm ->
            ( { model | searchTerm = searchTerm }, Cmd.none )



-- VIEW


view : Model -> { title : String, body : List (Html Msg) }
view model =
    { title = "Spotifwdy"
    , body =
        [ main_ [ class "main" ]
            [ header []
                [ h1 [] [ text "Spotifwdy" ]
                ]
            , div [ class "page-container" ]
                [ Status.viewSuccess model.artists <|
                    \artists ->
                        viewSearchField model.searchTerm artists <|
                            viewArtists
                ]
            ]
        ]
    }


viewArtists : List Artist -> Html Msg
viewArtists artists =
    ul [ class "artists" ] <|
        List.map viewArtist artists


viewArtist : Artist -> Html Msg
viewArtist artist =
    a [ href <| Route.toUrl (Route.Artist artist.id) ]
        [ li [ class "artist" ] [ text artist.name ]
        ]


viewSearchField : String -> List Artist -> (List Artist -> Html Msg) -> Html Msg
viewSearchField searchTerm xs f =
    let
        xs_ =
            List.filterWithSearchTerm searchTerm [ .name ] xs
    in
    div [ class "searchable-container" ] <|
        [ input
            [ type_ "text"
            , value searchTerm
            , onInput OnSearchTermInput
            , class "searchable-container__input"
            ]
            []
        , f xs_
        ]
