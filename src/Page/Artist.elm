module Page.Artist exposing
    ( Model
    , Msg(..)
    , init
    , subscriptions
    , update
    , view
    )

import Data.Artist as Artist exposing (Artist)
import Data.Id as Id
import Data.Song as Song exposing (Song)
import Data.Status as Status exposing (Status(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Util.List as List



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- MODEL


type alias Model =
    { artistId : Id.ArtistId
    , artist : Status Http.Error Artist
    , searchTerm : String
    , songs : Status Http.Error (List Song)
    }


init : Id.ArtistId -> ( Model, Cmd Msg )
init artistId =
    ( { artistId = artistId
      , artist = Loading
      , searchTerm = ""
      , songs = Loading
      }
    , Artist.get artistId GotArtist
    )



-- UPDATE


type Msg
    = NoOp
    | GotArtist (Result Http.Error Artist)
    | GotSongs (Result Http.Error (List Song))
    | OnSearchTermInput String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        GotArtist (Ok artist) ->
            ( { model
                | artist =
                    Status.Success artist
              }
            , Song.getArtistCollection artist.name GotSongs
            )

        GotArtist (Err error) ->
            ( { model
                | artist =
                    Status.Failure error
              }
            , Cmd.none
            )

        GotSongs result ->
            ( { model | songs = Status.fromResult result }
            , Cmd.none
            )

        OnSearchTermInput searchTerm ->
            ( { model | searchTerm = searchTerm }, Cmd.none )



-- VIEW


view : Model -> { title : String, body : List (Html Msg) }
view model =
    { title = "Spotifwdy"
    , body =
        [ main_ [ class "main" ]
            [ div [ class "page-container" ]
                [ Status.viewSuccess2 ( model.artist, model.songs ) <|
                    \( artist, songs ) ->
                        header []
                            [ h1 []
                                [ text artist.name ]
                            , viewSearchField model.searchTerm songs <|
                                viewSongs
                            ]
                ]
            ]
        ]
    }


viewSongs : List Song -> Html Msg
viewSongs songs =
    ul [ class "songs" ] <|
        List.map viewSong songs


viewSong : Song -> Html Msg
viewSong song =
    li [ class "song" ]
        [ text song.name
        ]


viewSearchField : String -> List Song -> (List Song -> Html Msg) -> Html Msg
viewSearchField searchTerm xs f =
    let
        xs_ =
            List.filterWithSearchTerm searchTerm [ .name ] xs
    in
    div [ class "searchable-container" ] <|
        [ input
            [ type_ "text"
            , value searchTerm
            , onInput OnSearchTermInput
            , class "searchable-container__input"
            ]
            []
        , f xs_
        ]
