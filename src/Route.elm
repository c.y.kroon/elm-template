module Route exposing
    ( Route(..)
    , fromUrl
    , toUrl
    )

import Data.Id as Id
import Url exposing (Url)
import Url.Builder
import Url.Parser as Parser exposing ((</>), Parser, oneOf)



-- Routing


type Route
    = Home
    | Artist Id.ArtistId
    | NotFound


parser : Parser (Route -> a) a
parser =
    oneOf
        [ Parser.map Artist (Parser.s "artists" </> Id.fromUrl)
        , Parser.map Home Parser.top
        ]



-- PUBLIC HELPERS


fromUrl : Url -> Route
fromUrl url =
    Maybe.withDefault NotFound <|
        Parser.parse parser url


toUrl : Route -> String
toUrl route =
    let
        ( segments, queryParams ) =
            toPathSegments route
    in
    Url.Builder.absolute segments queryParams


toPathSegments : Route -> ( List String, List Url.Builder.QueryParameter )
toPathSegments route =
    case route of
        Home ->
            ( [], [] )

        Artist artistId ->
            ( [ "artists", Id.toString artistId ], [] )

        NotFound ->
            ( [ "not-found" ], [] )
