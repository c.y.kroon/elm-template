module Component.Loader exposing
    ( viewLoader
    , viewPageLoader
    )

import Html exposing (..)
import Html.Attributes exposing (..)



-- LOADER VIEWS


viewLoader : Html msg
viewLoader =
    -- FIXME: Add loading spinner
    div [ class "loading-spinner" ] [ text "Loading..." ]


viewPageLoader : Html msg
viewPageLoader =
    div [ class "loading-page-container" ] [ viewLoader ]
