module Data.Status exposing
    ( Status(..)
    , fromResult
    , viewSuccess
    , viewSuccess2
    )

import Component.Loader as Loader
import Html exposing (..)
import Http


type Status err a
    = NotAsked
    | Loading
    | Failure err
    | Success a



-- HELPER FUNCTIONS


{-| Maps the Result type to the Status type
-}
fromResult : Result err a -> Status err a
fromResult result =
    case result of
        Ok x ->
            Success x

        Err err ->
            Failure err



-- VIEW FUNCTIONS


viewSuccess : Status Http.Error a -> (a -> Html msg) -> Html msg
viewSuccess status f =
    case status of
        NotAsked ->
            text ""

        Loading ->
            Loader.viewPageLoader

        Failure error ->
            -- FIXME: handle error
            text "Http error"

        Success a ->
            f a


viewSuccess2 :
    ( Status Http.Error a, Status Http.Error b )
    -> (( a, b ) -> Html msg)
    -> Html msg
viewSuccess2 status f =
    case status of
        ( Success a, Success b ) ->
            f ( a, b )

        ( Loading, Loading ) ->
            Loader.viewPageLoader

        ( Failure _, _ ) ->
            -- FIXME: handle error
            text "Http error"

        ( _, Failure _ ) ->
            -- FIXME: handle error
            text "Http error"

        ( Loading, _ ) ->
            Loader.viewPageLoader

        _ ->
            text ""
