module Data.Artist exposing (..)

import Data.Id as Id
import Endpoint
import Http
import Json.Decode
import Json.Decode.Pipeline


type alias Artist =
    { id : Id.ArtistId
    , name : String
    }



-- ENDPOINTS


getCollection : (Result Http.Error (List Artist) -> msg) -> Cmd msg
getCollection toMsg =
    Http.get
        { url = Endpoint.toUrl Endpoint.artists
        , expect =
            Http.expectJson toMsg <|
                Json.Decode.list decoder
        }


get : Id.ArtistId -> (Result Http.Error Artist -> msg) -> Cmd msg
get artistId toMsg =
    Http.get
        { url = Endpoint.toUrl (Endpoint.artist artistId)
        , expect =
            Http.expectJson toMsg decoder
        }



-- DECODING


decoder : Json.Decode.Decoder Artist
decoder =
    Json.Decode.succeed Artist
        |> Json.Decode.Pipeline.required "id" Id.decoder
        |> Json.Decode.Pipeline.required "name" Json.Decode.string
