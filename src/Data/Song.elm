module Data.Song exposing (..)

import Data.Id as Id
import Endpoint
import Http
import Json.Decode as Decode
import Json.Decode.Pipeline as Pipeline


type alias Song =
    { id : Id.SongId
    , name : String
    , year : Int
    , artist : String
    , shortname : String
    , bpm : Maybe Int
    , duration : Int
    , genre : String
    , spotifyId : Maybe String
    , album : Maybe String
    }



-- ENDPOINTS


getArtistCollection : String -> (Result Http.Error (List Song) -> msg) -> Cmd msg
getArtistCollection artistName toMsg =
    Http.get
        { url = Endpoint.toUrl (Endpoint.songs artistName)
        , expect =
            Http.expectJson toMsg <|
                Decode.list decoder
        }



-- DECODING


decoder : Decode.Decoder Song
decoder =
    Decode.succeed Song
        |> Pipeline.required "id" Id.decoder
        |> Pipeline.required "name" Decode.string
        |> Pipeline.required "year" Decode.int
        |> Pipeline.required "artist" Decode.string
        |> Pipeline.required "shortname" Decode.string
        |> Pipeline.required "bpm" (Decode.nullable Decode.int)
        |> Pipeline.required "duration" Decode.int
        |> Pipeline.required "genre" Decode.string
        |> Pipeline.required "spotifyId" (Decode.nullable Decode.string)
        |> Pipeline.required "album" (Decode.nullable Decode.string)
