module Data.Id exposing
    ( ArtistId
    , Id
    , SongId
    , decoder
    , encode
    , fromString
    , fromUrl
    , toString
    )

import Json.Decode as Decode
import Json.Encode as Encode
import Url.Parser as Parser


{-| A phantom type is a parametrised type whose parameters
do not all appear on the right-hand side of its definition.

Read more: <https://dmalashock.com/elm-phantom-types/>

Using the Id type like this let's us use functions like `toString`
on any Id regardless of its `a` contraint.

-}



-- DEFINITIONS


type Id a
    = Id Int


type alias ArtistId =
    Id { artistId : () }


type alias SongId =
    Id { songId : () }



-- PUBLIC HELPERS


toInt : Id a -> Int
toInt (Id id) =
    id


toString : Id a -> String
toString =
    String.fromInt << toInt


fromString : String -> Maybe (Id a)
fromString =
    Maybe.map Id << String.toInt


fromUrl : Parser.Parser (Id b -> a) a
fromUrl =
    Parser.map Id Parser.int


decoder : Decode.Decoder (Id a)
decoder =
    Decode.map Id Decode.int


encode : Id a -> Encode.Value
encode (Id id) =
    Encode.int id
