module Util.List exposing (filterWithSearchTerm)

{-| Filter a list based on a searchTerm and a list of searchable parameters. Eg.

    ```filterWithSearchTerm "John" [.name, .email] usersList```

    Will expect a list of records with a name and email field.
    It will return a list of all records with either the email or name field matching the searchterm.

-}


filterWithSearchTerm : String -> List (a -> String) -> List a -> List a
filterWithSearchTerm searchTerm listToCompare xs =
    let
        -- Remove whitespace, lowercase searchterm
        queryTerms =
            String.words (String.toLower searchTerm)

        matchesAllTerms x =
            let
                -- Lowercase all list item parameters that will be compared
                lowerListToCompare =
                    List.map
                        (\toCompare -> String.toLower (toCompare x))
                        listToCompare

                -- Returns true if any listItem parameter matches single word in the search term
                matchesTerm term =
                    List.any (String.contains term) lowerListToCompare
            in
            -- Returns true if a listItem parameter matches all words in the search term
            List.all matchesTerm queryTerms
    in
    if List.isEmpty listToCompare then
        -- Return complete list if no searchable parameters are given
        xs

    else
        -- Return filtered list
        List.filter matchesAllTerms xs
