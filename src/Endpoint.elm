module Endpoint exposing
    ( Endpoint
    , artist
    , artists
    , songs
    , toUrl
    )

import Data.Id as Id
import Url.Builder exposing (QueryParameter)


{-| The Endpoint type wraps API urls, to only allow for
predefined urls and ensure the same base url.
-}
type Endpoint
    = Endpoint String



-- BASE URL


endpoint : List String -> List QueryParameter -> Endpoint
endpoint segments queryParameters =
    Endpoint
        (Url.Builder.crossOrigin
            "http://localhost:4000"
            segments
            queryParameters
        )



-- ENDPOINTS


artist : Id.ArtistId -> Endpoint
artist artistId =
    endpoint [ "artists", Id.toString artistId ] []


artists : Endpoint
artists =
    endpoint [ "artists" ] []


songs : String -> Endpoint
songs artistName =
    let
        query =
            Url.Builder.string "artist" artistName
    in
    endpoint [ "songs" ] [ query ]



-- PUBLIC HELPER FUNCTIONS


toUrl : Endpoint -> String
toUrl (Endpoint url) =
    url
